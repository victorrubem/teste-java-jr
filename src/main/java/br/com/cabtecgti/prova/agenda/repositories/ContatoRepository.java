package br.com.cabtecgti.prova.agenda.repositories;

import java.util.Collections;
import java.util.List;

import javax.ejb.DependsOn;
import javax.ejb.Stateless;

import br.com.cabtecgti.prova.agenda.entities.Contato;
import br.com.cabtecgti.prova.agenda.entities.Recado;

/**
 * Repositório de contatos.
 * 
 * @author Cabtec GTI
 *
 */
@Stateless
@DependsOn("DatabaseMigrations")
public class ContatoRepository extends AbstractRepository<Contato, Long> {

    public ContatoRepository() {
        super(Contato.class);
    }

    public List<Recado> recadosNaoLidos(final Long idContato) {
        return Collections.emptyList();
    }
}