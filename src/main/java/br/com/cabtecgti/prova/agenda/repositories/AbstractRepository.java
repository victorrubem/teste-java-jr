package br.com.cabtecgti.prova.agenda.repositories;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.cabtecgti.prova.agenda.entities.MasterEntity;

/**
 * Ancestral para as classes repositório do projeto. Já recebe o EntityManager por injeção.
 * 
 * @author Cabtec GTI
 *
 * @param <E>
 *            Entidade
 * @param <ID>
 *            Tipo do ID da entidade
 */
public abstract class AbstractRepository<E extends MasterEntity, ID extends Serializable> {

    @PersistenceContext(unitName = "agenda")
    protected EntityManager em;

    private Class<E> entityClass;

    public AbstractRepository(final Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * Insere um novo registro.
     * 
     * @param entity
     *            a entidade a ser salva.
     * @return A entidade criada.
     */
    public E create(final E entity) {
        em.persist(entity);
        return entity;
    }

    /**
     * Atualiza uma entidade existente.
     * 
     * @param entity
     *            a entidade a ser atualizada.
     * @return A entidade atualizada.
     */
    public E update(final E entity) {
        return em.merge(entity);
    }

    /**
     * Remove a entidade.
     * 
     * @param entity
     *            A entidade a ser removida.
     */
    public void delete(final E entity) {
        final E toDelete = em.merge(entity);
        em.remove(toDelete);
    }

    /**
     * Recupera uma entidade pelo ID.
     * 
     * @param id
     *            Id da entidade a ser recuperada
     * @return A entidade. Nulo se não existir entidade para o ID informado.
     */
    public E findById(final ID id) {
        return em.find(entityClass, id);
    }

    /**
     * Executa pesquisa de entidades.
     * 
     * @param filter
     * @param offset
     * @param limit
     * @return
     */
    public ResultList<E> search(final Object filter, final int offset, final int limit) {
        final String queryCount =
            new StringBuilder("SELECT COUNT(*) FROM ").append(entityClass.getName()).toString();
        final int count = em.createQuery(queryCount, Long.class).getSingleResult().intValue();

        if (count > 0) {
            final String query = new StringBuilder("FROM ").append(entityClass.getName()).toString();
            final List<E> result =
                em.createQuery(query, entityClass)
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .getResultList();
            return new ResultList<>(result, count, offset, limit);
        } else {
            return new ResultList<>(Collections.emptyList(), count, offset, limit);
        }
    }
}