package br.com.cabtecgti.prova.agenda.controllers;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import br.com.cabtecgti.prova.agenda.entities.Contato;
import br.com.cabtecgti.prova.agenda.repositories.ContatoRepository;
import br.com.cabtecgti.prova.agenda.repositories.ResultList;

@Named
@br.com.cabtecgti.faces.bean.ViewScoped
public class ContatosAction extends BaseAction {

    private static final long serialVersionUID = 1L;

    @EJB
    private ContatoRepository repo;

    public ContatosAction() {
        super("contatos");
        setSelectListener(new ContatoSelected());
    }

    private class ContatoSelected implements SelectListener {
        @Override
        public void selected(final Object selected) {
            final Contato contato = (Contato) selected;
            ContatosAction.this.navigateToEdit("contatos-edit.xhtml", contato.getId());
        }
    }

    @Override
    protected ResultList<Contato> doSearch() {
        return repo.search(null, getOffset(), getLimit());
    }

    @Override
    public void create() {
        setEntity(new Contato());
    }

    @Override
    public void edit(final Object id) {
        final Contato contato = repo.findById((Long) id);
        setEntity(contato);
        if (contato == null) {
            FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Registro não encontrado.", null));
        }
    }

    @Override
    public void save() {
        final Contato entity = (Contato) getEntity();
        String msg = null;
        if (entity.getId() != null) {
            setEntity(repo.update(entity));
            msg = "Registro salvo com sucesso.";
        } else {
            setEntity(repo.create(entity));
            msg = "Registro criado com sucesso.";
        }
        FacesContext.getCurrentInstance().addMessage(null,
            new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null));
    }

}
