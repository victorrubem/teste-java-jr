package br.com.cabtecgti.prova.agenda.repositories;

import javax.ejb.DependsOn;
import javax.ejb.Stateless;

import br.com.cabtecgti.prova.agenda.entities.Recado;

/**
 * Repositório de recados.
 * 
 * @author Cabtec GTI
 *
 */
@Stateless
@DependsOn("DatabaseMigrations")
public class RecadoRepository extends AbstractRepository<Recado, Long> {

    public RecadoRepository() {
        super(Recado.class);
    }

}